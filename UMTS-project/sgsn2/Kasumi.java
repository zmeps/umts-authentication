/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sgsn2;

/**
 *
 * @author sakisba
 */
public class Kasumi {
   
	// Instance Variables
	// Constants used to XOR with keyJ in order to get keyJPrime {C1,...,C8}
	private final int[] KEYCONSTANTS = { 
			0x0123, 0x4567, 0x89ab, 0xcdef,
			0xfedc, 0xba98, 0x7654, 0x3210 };
	/**
	 * @property plaintext	Message to be encrypted
	 */
	private long plaintext; //Message to be encrypted
	
	// Keys variables K and K'
	private int[] keyJ = new int[8];
	private int[] keyJPrime = new int[8];
	// Sub-keys variables KL, KO and KI
	int[][] subkeyL = new int[8][2];
	int[][] subkeyO = new int[8][3];
	int[][] subkeyI = new int[8][3];

	/**
	 * Constructor; starts key schedule
	 * 
	 * @param keyLeft
	 * @param keyRight
	 */
	public Kasumi(long keyLeft, long keyRight) {

		// Variable used to select chunks of 16-bits from K1 to K8
		long keySlot = 0xffff000000000000L;
		int shift = 48; // Used to move between slots		
		
		for (int i = 0; i < 4; i++) {
			keyJ[i] = (int) (((keyLeft & keySlot) >>> shift));
			
			keyJ[i + 4] = (int) (((keyRight & keySlot) >>> shift));
			
			keySlot = keySlot >>> 16; // Move to the next key slot
			shift -= 16;
		}// End for-loop
		
		//Run KeySchedule to generate the sub-keys.
		keySchedule();
	}// End of Kasumi(long, long)
		
     /**
	 * Generates subkeys.
	 */
	public void keySchedule() {

		// Create second array of subkeys by XOR with KEYCONSTANTS
		for (int i = 0; i < 8; i++) {
			keyJPrime[i] = keyJ[i] ^ KEYCONSTANTS[i];
		}

		// Generate function round keys
		// See Table 1:Round subkeys on page 14 of KASUMI 3GPP spec
		for (int i = 0; i < 8; i++) {
			
			subkeyL[i][0] = (keyJ[i] << 1 | keyJ[i] >>> 15) & 0xFFFF;		
			subkeyL[i][1] = keyJPrime[(i + 2) % 8];
			
			subkeyO[i][0] = (keyJ[(i + 1) % 8] << 5 | keyJ[(i + 1) % 8] >>> 11) & 0xFFFF;
			subkeyO[i][1] = ((keyJ[(i + 5) % 8] << 8 | keyJ[(i + 5) % 8] >>> 8)) & 0xFFFF;
			subkeyO[i][2] = ((keyJ[(i + 6) % 8] << 13 | keyJ[(i + 6) % 8] >>> 3)) & 0xFFFF;
			
			subkeyI[i][0] = keyJPrime[(i + 4) % 8];
			subkeyI[i][1] = keyJPrime[(i + 3) % 8];
			subkeyI[i][2] = keyJPrime[(i + 7) % 8];
			
		}//end of for-loop
	}// end of KeySchedule()
    
	/**
	 * Perform KASUMI encryption
	 * @param plaintext	Message to be encrypted
	 * @param rounds	Number of rounds; variable for attack, otherwise use 8 for encryption
	 * @return	Derived ciphertext
	 */
	public long encrypt(long plaintext, int rounds){
		
		this.plaintext = plaintext;
		int left = (int)(plaintext >>> 32);
		int right= (int)(plaintext & 0xFFFFFFFF);
		int prevRight;
		
		for(int i = 1; i <= rounds; i++){
			
			prevRight = right;
			right = left;
			
			if(i % 2 != 0){
				left = prevRight ^ FO(FL(left, i - 1), i - 1);
			}
			else{
				left = prevRight ^ FL(FO(left, i - 1), i - 1);
			}
			
		}
		return (long)left << 32 | ((long)right & 0x00000000FFFFFFFFL);
	}
	
	/**
	 * Perform KASUMI decryption.
	 * @param ciphertext	Encrypted message
	 * @param rounds	Number of rounds; variable for attack, otherwise use 8 to decrypt
	 * @return	derived plaintext
	 */
	public long decrypt(long ciphertext, int rounds){
		int left = (int)(ciphertext >>> 32);
		int right= (int)(ciphertext & 0xFFFFFFFF);
		int prevLeft;
		
		System.out.printf("functioni - L" + 0 + "   = %016X%n", left);
		System.out.printf("functioni - R" + 0 + "   = %016X%n", right);
		
		for(int i = rounds; i >= 1; i--){
			
			prevLeft = left;
			left = right;
			
			if(i % 2 != 0){
				right = prevLeft ^ FO(FL(right, i - 1), i - 1);
			}
			else{
				right = prevLeft ^ FL(FO(right, i - 1), i - 1);
			}
			
		}
		
		return (long)left << 32 | ((long)right & 0x00000000FFFFFFFFL);
	}
	
	/**
	 * Method used for implement Function FL for KASUMI Cipher
	 * 
	 * @param in32Bits
	 * @param round
	 * @return out32Bits
	 */
	public int FL(int in32Bits, int round)
	{
		// Local variables
		int L0, L1, R0, R1;

		// Split the 32-Bit input I in 2 half, 16-bits each
		R0 = in32Bits & 0xffff;
		L0 = (in32Bits >>> 16) & 0xffff;
	
		// Running Function FL as described on page 10 of KASUMI 3GPP spec
		R1 = R0 ^ (((L0 & subkeyL[round][0]) << 1 | (L0 & subkeyL[round][0]) >>> 15) & 0xFFFF);	
		L1 = L0 ^ (((R1 | subkeyL[round][1]) << 1 | (R1 | subkeyL[round][1]) >>> 15) & 0xFFFF);
				
		// Merge results for output
		int out32Bits = (L1 << 16) | R1;

		// return out16Bits;
		return out32Bits;
	}// end of FL(int, int)
	
	
	/**
	 * Method used for implement Function FO for KASUMI Cipher
	 * 
	 * @param in32Bits
	 * @param round
	 * @return out32Bits
	 */
	public int FO(int in32Bits, int round) {
		// Local variables
		int L0, L1, L2, L3, R0, R1, R2, R3;

		// Split the 32-Bit input I in 2 half, 16-bits each
		R0 = in32Bits & 0xffff;
		L0 = (in32Bits >>> 16 & 0xffff);
		
		// Running Function FO as described on page 11 of KASUMI 3GPP spec
		R1 = FI((L0 ^ subkeyO[round][0]), subkeyI[round][0]) ^ R0;
		L1 = R0;
		
		R2 = FI((L1 ^ subkeyO[round][1]), subkeyI[round][1]) ^ R1;
		L2 = R1;
		
		R3 = FI((L2 ^ subkeyO[round][2]), subkeyI[round][2]) ^ R2;
		L3 = R2;
		
		// Merge results for output
		int out32Bits = (L3 << 16) | R3;
		
		//return out16Bits;
		return out32Bits;
	}// end of FO(int, long, long)

	/**
	 * Function that takes 7-bit input and converts it to 9-bit output
	 * 
	 * @param in7Bits
	 * @return out9Bits
	 */
	private int ZE(int in7Bits) {
		int out9Bits = in7Bits & 0x1FF;
		return out9Bits;
	}// end of ZE(int)
	
	/**
	 * Function that takes 9-bit input and converts it to 7-bit output
	 * 
	 * @param in9Bits
	 * @return out7Bits
	 */
	private int TR(int in9Bits) {
		int out7Bits = in9Bits & 0x7F;
		return out7Bits;
	}// end of TR(int)
	
	/**
	 * Method used for implement Function FI for KASUMI Cipher
	 * 
	 * @param in16Bits
	 * @param subkey
	 * @return out16Bits
	 */
	public int FI(int in16Bits, int subkey) {
		// Local variables
		int L0, L1, L2, L3, L4, R0, R1, R2, R3, R4, key9Bits, key7Bits;

		// Split the input I in 2 half, 9-bit left and 7-bit right
		R0 = in16Bits & 0x7f;
		L0 = (in16Bits >>> 7 & 0x1FF);
			
		// Split the key in 2 half, 7-bit left and 9-bit right
		key9Bits = subkey & 0x1FF;
		key7Bits = (subkey >>> 9 & 0x7f);

		// Running Function FI as described on page 11 of KASUMI 3GPP spec
		L1 = R0;
		R1 = S9(L0) ^ ZE(R0);
		L2 = R1 ^ key9Bits;
		R2 = S7(L1) ^ TR(R1) ^ key7Bits;
		L3 = R2;
		R3 = S9(L2) ^ ZE(R2);
		L4 = S7(L3) ^ TR(R3);
		R4 = R3;
		
		// Merge results for output
		int out16Bits = (L4 << 9) | R4;
		
		return out16Bits;
	}// end of FI(int, int)
	
	
	
	/**
	 * S-Box S7 for KASUMI Block Cipher
	 * @param in7Bits
	 * @return out7Bits
	 */
	private int S7(int in7Bits) {
		// Extract bits from input to be used by S7 Block
		int x0 = (in7Bits & 1), 
			x1 = (in7Bits >> 1) & 1, 
			x2 = (in7Bits >> 2) & 1, 
			x3 = (in7Bits >> 3) & 1, 
			x4 = (in7Bits >> 4) & 1, 
			x5 = (in7Bits >> 5) & 1, 
			x6 = (in7Bits >> 6) & 1;

		// S7 Block Gate Logic
		// See 4.5.1: Gate Logic on page 12 of KASUMI 3GPP spec
		int y0 = (x1 & x3 ^ x4 ^ x0 & x1 & x4 ^ x5 ^ x2 & x5 ^ x3 & x4 & x5
				^ x6 ^ x0 & x6 ^ x1 & x6 ^ x3 & x6 ^ x2 & x4 & x6 ^ x1 & x5
				& x6 ^ x4 & x5 & x6);

		int y1 = (x0 & x1 ^ x0 & x4 ^ x2 & x4 ^ x5 ^ x1 & x2 & x5 ^ x3 & x5
				^ x6 ^ x0 & x2 & x6 ^ x3 & x6 ^ x4 & x5 & x6 ^ 1);

		int y2 = (x0 ^ x0 & x3 ^ x2 & x3 ^ x1 & x2 & x4 ^ x0 & x3 & x4 ^ x1
				& x5 ^ x0 & x2 & x5 ^ x0 & x6 ^ x0 & x1 & x6 ^ x2 & x6 ^ x4
				& x6 ^ 1);

		int y3 = (x1 ^ x0 & x1 & x2 ^ x1 & x4 ^ x3 & x4 ^ x0 & x5 ^ x0 & x1
				& x5 ^ x2 & x3 & x5 ^ x1 & x4 & x5 ^ x2 & x6 ^ x1 & x3 & x6);

		int y4 = (x0 & x2 ^ x3 ^ x1 & x3 ^ x1 & x4 ^ x0 & x1 & x4 ^ x2 & x3
				& x4 ^ x0 & x5 ^ x1 & x3 & x5 ^ x0 & x4 & x5 ^ x1 & x6 ^ x3
				& x6 ^ x0 & x3 & x6 ^ x5 & x6 ^ 1);

		int y5 = (x2 ^ x0 & x2 ^ x0 & x3 ^ x1 & x2 & x3 ^ x0 & x2 & x4 ^ x0
				& x5 ^ x2 & x5 ^ x4 & x5 ^ x1 & x6 ^ x1 & x2 & x6 ^ x0 & x3
				& x6 ^ x3 & x4 & x6 ^ x2 & x5 & x6 ^ 1);

		int y6 = (x1 & x2 ^ x0 & x1 & x3 ^ x0 & x4 ^ x1 & x5 ^ x3 & x5 ^ x6
				^ x0 & x1 & x6 ^ x2 & x3 & x6 ^ x1 & x4 & x6 ^ x0 & x5 & x6);

		// Merge results for output
		int out7Bits = (y6 << 6 | y5 << 5 | y4 << 4 | y3 << 3 | y2 << 2 | y1 << 1 | y0);
		
		return out7Bits;
	}// end of S7(int)

	/**
	 * S-Box S9 for KASUMI Block Cipher 
	 * @param in9Bits
	 * @return out9Bits
	 */
	private int S9(int in9Bits) {
		// Extract bits from input to be used by S9 Block
		int x0 = (in9Bits & 1), 
			x1 = (in9Bits >> 1) & 1, 
			x2 = (in9Bits >> 2) & 1, 
			x3 = (in9Bits >> 3) & 1, 
			x4 = (in9Bits >> 4) & 1, 
			x5 = (in9Bits >> 5) & 1, 
			x6 = (in9Bits >> 6) & 1, 
			x7 = (in9Bits >> 7) & 1, 
			x8 = (in9Bits >> 8) & 1;

		// S9 Block Gate Logic
		// See 4.5.2: Gate Logic on page 13 of KASUMI 3GPP spec
		int y0 = (x0 & x2 ^ x3 ^ x2 & x5 ^ x5 & x6 ^ x0 & x7 ^ x1 & x7 ^ x2
				& x7 ^ x4 & x8 ^ x5 & x8 ^ x7 & x8 ^ 1);

		int y1 = (x1 ^ x0 & x1 ^ x2 & x3 ^ x0 & x4 ^ x1 & x4 ^ x0 & x5 ^ x3
				& x5 ^ x6 ^ x1 & x7 ^ x2 & x7 ^ x5 & x8 ^ 1);

		int y2 = (x1 ^ x0 & x3 ^ x3 & x4 ^ x0 & x5 ^ x2 & x6 ^ x3 & x6 ^ x5
				& x6 ^ x4 & x7 ^ x5 & x7 ^ x6 & x7 ^ x8 ^ x0 & x8 ^ 1);

		int y3 = (x0 ^ x1 & x2 ^ x0 & x3 ^ x2 & x4 ^ x5 ^ x0 & x6 ^ x1 & x6
				^ x4 & x7 ^ x0 & x8 ^ x1 & x8 ^ x7 & x8);

		int y4 = (x0 & x1 ^ x1 & x3 ^ x4 ^ x0 & x5 ^ x3 & x6 ^ x0 & x7 ^ x6
				& x7 ^ x1 & x8 ^ x2 & x8 ^ x3 & x8);

		int y5 = (x2 ^ x1 & x4 ^ x4 & x5 ^ x0 & x6 ^ x1 & x6 ^ x3 & x7 ^ x4
				& x7 ^ x6 & x7 ^ x5 & x8 ^ x6 & x8 ^ x7 & x8 ^ 1);

		int y6 = (x0 ^ x2 & x3 ^ x1 & x5 ^ x2 & x5 ^ x4 & x5 ^ x3 & x6 ^ x4
				& x6 ^ x5 & x6 ^ x7 ^ x1 & x8 ^ x3 & x8 ^ x5 & x8 ^ x7 & x8);

		int y7 = (x0 & x1 ^ x0 & x2 ^ x1 & x2 ^ x3 ^ x0 & x3 ^ x2 & x3 ^ x4
				& x5 ^ x2 & x6 ^ x3 & x6 ^ x2 & x7 ^ x5 & x7 ^ x8 ^ 1);

		int y8 = (x0 & x1 ^ x2 ^ x1 & x2 ^ x3 & x4 ^ x1 & x5 ^ x2 & x5 ^ x1
				& x6 ^ x4 & x6 ^ x7 ^ x2 & x8 ^ x3 & x8);

		// Merge results for output
		int out9Bits = (y8 << 8 | y7 << 7 | y6 << 6 | y5 << 5 | y4 << 4 | y3 << 3
				| y2 << 2 | y1 << 1 | y0);
		
		return out9Bits;
	}// end of S9(int)
	
	/**
	 * The main program
	 * 
	 * @param args
	 */
	/*
         public static void main(String[] args) {
		long keyLeft = 0x0123456789abcdefL;
		long keyRight = 0x0123456789abcdefL;

		Kasumi k = new Kasumi(keyLeft, keyRight);
		
		System.out.printf("Decryption: = %016X%n", k.decrypt(k.encrypt(0x0123456789abcdefL, 8),8));
	}// end of main program
         
         */
}// end of kasumi Class
