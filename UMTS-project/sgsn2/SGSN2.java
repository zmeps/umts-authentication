/*
*
*
* @authors                              Bazakas Athanasios 	icsdm11034 
* 					Gorogia Argyrw		icsdm11005
*					Karampelas Loukas	icsdm11008	
* 					Tsiatsikas Zisis 	icsdm11004	
*					
*/
package sgsn2;
import java.net.*;
import java.util.StringTokenizer;
import java.io.*;
import java.util.ArrayList;
public class SGSN2 {

            private ArrayList <Long> TMSI;
            private long TMSI_long = 1234567890;
             
             	   
	    ServerSocket m_ServerSocket=null;
	    Socket clientSocket = null;
	    
	    int Server1Port = 11111;
	    String Server2IP = "127.0.0.1";
	    //String Server2IP = "192.168.2.101";
	    //String Server2IP = "10.10.10.185";
	    int Server2Port = 22222;
	    
	   
	    public SGSN2() 
	    {	
	    	
	    	
		    try
		       {
                            //Client Socket
                            clientSocket = new Socket(Server2IP, Server2Port);
		       }//try       
		      
		      catch(UnknownHostException uhe)
		       
		       {
		           // Host unreachable
		           System.out.println("Unknown Host");
		           clientSocket = null;
		       }//catch
		 
		        
		       catch(IOException ioe)
		       {
		           // Cannot connect to port on given host
		           System.out.println("Cant connect to server at "+Server2Port+". Make sure it is running.");
		           clientSocket = null;
		       }//catch
		       
		       if(clientSocket == null)
		           System.exit(-1);
				
				System.out.println("Connected to Server2.");
				
	        try
	        {
	            // Create the server socket.
	            m_ServerSocket = new ServerSocket(Server1Port);
	        }//try
	        catch(IOException ioe)
	        {
	            System.out.println("Could not create server socket at "+Server1Port+". Quitting.");
	            System.exit(-1);
	        }//catch
	        
	        System.out.println("Listening for clients on "+Server1Port+"...");
	        
	        // Successfully created Server Socket. Now wait for connections.
	        int id = 0;
                                              
	        while(true)
	        {                        
	            try
	            {
	                // Accept incoming connections.
	                Socket clientSocket = m_ServerSocket.accept();
	            
                        
                        
                        
	                // Start a service thread                
	                ClientServiceThread cliThread = new ClientServiceThread(clientSocket, id++);
	                cliThread.start();
	            }//try
	            catch(IOException ioe)
	            {
	                System.out.println("Exception encountered on accept. Ignoring. Stack Trace :");
	                ioe.printStackTrace();
	            }//catch
	        }//while
	    }//constructor
	    
	    public static void main (String[] args)
	    {
	        new SGSN2();    
	    }//main
	    
	    
	    class ClientServiceThread extends Thread
	    {
	        Socket m_clientSocket;        
	        int m_clientID = -1;
	        boolean m_bRunThread = true;
	        
	        ClientServiceThread(Socket s, int clientID)
	        {
	            m_clientSocket = s;
	            m_clientID = clientID;
	        }//constructor thread
	       
	        public void run()
	        {            
                        //Streams UE
	        	ObjectInputStream inClient = null;
	     		ObjectOutputStream outClient = null;
	            	     	
                        //Streams HLR
	     		ObjectInputStream inServer2 = null;
	     		ObjectOutputStream outServer2 = null;
	     		
	            // Print out details of this connection
	            System.out.println("Client : ID - " + m_clientID + " : Address - " + 
	                             m_clientSocket.getInetAddress().getHostName()+" - Acceptedd");
	                
	            try
	            {   
	            	inClient = new ObjectInputStream(m_clientSocket.getInputStream());
	    	        outClient = new ObjectOutputStream(m_clientSocket.getOutputStream());
	       
	    	        outServer2 = new ObjectOutputStream(clientSocket.getOutputStream());
	    	        inServer2 = new ObjectInputStream(clientSocket.getInputStream());
	       
	    	        String id_IMSI = null;
	    	        String id_TMSI = null;
	    	        
	    	        while(m_bRunThread)
	                {  

	    	        	boolean ExitClientChoice=false;
	    	        	String FirstCommand=null;
	 			
	    	        	while(ExitClientChoice==false)
                                { 			
	    	        		
	    	        		FirstCommand = (String) inClient.readObject();
	    	        		   
	    	        		System.out.println("Client : ID - "+ m_clientID+" : Sent : " + FirstCommand);
	    	        		
	    	        		StringTokenizer token = new StringTokenizer(FirstCommand,"##");
	    	    	  	    FirstCommand=token.nextToken();//1d token
	    	    	  	    
	    	    	  	//Authentication
                                if (FirstCommand.equals("Authentication")) 
	                        {
	                          String SecondCommand = token.nextToken();//2d token
	                         
                                  //checking TMSI
	                          if (SecondCommand.equals("TMSI")) 
	                          {
	                              id_TMSI = token.nextToken();//3d token
	                              for(int i=0; i<TMSI.size(); i++)
	                              {
	                                  
	                                  if (id_TMSI.equals(TMSI.get(i)))
	                                  {
	                                      System.out.println("Client : ID - "+ m_clientID+" : Authenticcate Succesfully : id_TMSI : "+id_TMSI+" found!");
	                                  
                                          }//if 
                                          else 
                                          {
                                              System.out.println("Client : ID - "+ m_clientID+" : Authenticcation fail : id_TMSI : "+id_TMSI+" not found!!");
                                          
                                          }//else
	                             }//for   
	                           }//if SecondCommand TMSI
	                          else //(SecondCommand.equals("IMSI"))
	                          {
	                              id_IMSI = token.nextToken();//2d token
	                              System.out.println("Client : ID - "+m_clientID+" : Try to Authenticate...");
	                              outServer2.writeObject("AUTHENTICATION_REQ##"+id_IMSI); 
	                              outServer2.flush();
	                              
	                              ////////Server2 Response
	                              String HLR_Authentication_Response = (String) inServer2.readObject();
	                              System.out.println("HLR Sent : AUTHENTICATION DATA RESPONSE: "+HLR_Authentication_Response);		
	                              ///////Server2 Response
	                              
	                              if(HLR_Authentication_Response.equals("InvalidVector"))
	                              {
	                                  System.out.println("Client : ID - "+m_clientID+" : Invalid IMSI...");
	                                  outClient.writeObject("InvalidVector");
	                                  outClient.flush();
	                              }//if InvalidnVector
	                              else //(HLR_Authentication_Response.equals("AuthenticationDataResponse"))
	                              {    
	                                   String RAND_String="";
                                           String AUTN_String="";
	                                   String XRES_String="";
                                           String CK_String="";
                                           String IK_String="";
                                           String USERAUTHENTICATIONREQ ="";
                                           
                                           short IK[] = new short [16];
                                           short CK[] = new short [16];
                                           short XRES[] = new short [16];
                                           short RES[] = new short [16];
	                                  
                                           
                                           StringTokenizer AuthenticationDataResponse = new StringTokenizer(HLR_Authentication_Response,"**");RAND_String = AuthenticationDataResponse.nextToken();
                                           AUTN_String = AuthenticationDataResponse.nextToken();
                                           XRES_String = AuthenticationDataResponse.nextToken();
                                           CK_String = AuthenticationDataResponse.nextToken();
                                           IK_String = AuthenticationDataResponse.nextToken();
                                           
                                           StringTokenizer XRES_token = new StringTokenizer(XRES_String,"$$");
                                           for (int tmp=0; tmp<16; tmp++) {
                                               XRES[tmp] = Short.parseShort(XRES_token.nextToken());
                                           }
                                           
                                           StringTokenizer CK_token = new StringTokenizer(CK_String,"$$");
                                           for (int tmp=0; tmp<16; tmp++) {
                                               CK[tmp] = Short.parseShort(CK_token.nextToken());
                                           }
                                           
                                           StringTokenizer IK_token = new StringTokenizer(IK_String,"$$");
                                           for (int tmp=0; tmp<16; tmp++) {
                                               IK[tmp] = Short.parseShort(IK_token.nextToken());
                                           }
                                           
                                           
                                           
                                           System.out.println("------RAND---------"+RAND_String);
                                           System.out.println("------AUTN---------"+AUTN_String);
                                           System.out.println("------XRES---------"+XRES_String);
                                           System.out.println("------CK-----------"+CK_String);
                                           System.out.println("------IK-----------"+IK_String);
                                           
                                           
                                           USERAUTHENTICATIONREQ +=RAND_String+"**";
                                           USERAUTHENTICATIONREQ +=AUTN_String;
                                                                                      
	                                   System.out.println("USERAUTHENTICATION REQUEST - Sent to Client : ID - "+m_clientID+" (RAND ** AUTN) : "+USERAUTHENTICATIONREQ);                                     
	                                   outClient.writeObject(USERAUTHENTICATIONREQ);
	                                   outClient.flush();
                                           
                                           
                                           
                                           /* Comparing RES vs XRES */
                                           String RES_String = "";
                                           RES_String = (String) inClient.readObject();
                                           System.out.println("USER AUTHENTICATION RESPONSE - Client : ID - "+m_clientID+" : Sent (RES) : "+RES_String);
                                           
                                           StringTokenizer RES_token = new StringTokenizer(RES_String,"$$");
                                           for (int tmp=0; tmp<16; tmp++) {
                                               RES[tmp] = Short.parseShort(RES_token.nextToken());
                                           }
                                           
                                           boolean authentication = true;
                                           for (int tmp=0; tmp<16; tmp++) {
                                               if (RES[tmp] != XRES[tmp]) 
                                               authentication = false;
                                           }
                                           if (authentication==false)
                                                System.out.println("Client : ID - "+m_clientID+" - Fail to authenticate...");
                                           else
                                           {
                                               System.out.println("Client : ID - "+m_clientID+" - Authenticate succesfully!!");
                                               
                                               ////////////////////////////* Kasumi */////////////////////////////////////////////
                                             
                                               long keyLeft=1;
                                               long keyRight=1;
                                               long Crypto_TMSI;
                                             
                                               for (int tmp=0; tmp<8; tmp++) {
                                               keyLeft = (keyLeft * CK[tmp]);
                                               }//keyleft
                                               
                                               for (int tmp=8; tmp<16; tmp++) {
                                               keyLeft = (keyRight * CK[tmp]);
                                               }//keyrigth
                                              
                                              
                                               System.out.println("Generating TMSI for Client : ID - "+m_clientID+"... TMSI : "+TMSI_long);
                                               Kasumi k = new Kasumi(keyLeft, keyRight);
                                               Crypto_TMSI = k.encrypt(TMSI_long,8);
                                               
                                               System.out.println("TMSI : "+TMSI_long+" Encrypted with KASUMI Block Cipher... : "+Crypto_TMSI);
                                               
                                               //TMSI.add(TMSI_long);
                                               TMSI_long++;
                                               
                                               System.out.println("Sent to Client : ID - "+m_clientID+" - TMSI : "+Crypto_TMSI);
                                               
                                               outClient.writeObject(Crypto_TMSI);
                                               outClient.flush();
                                                                                      
                                           }//authentixation true
                                           
                                      }///(HLR_Authentication_Response.equals("AuthenticationDataResponse"))
	                          
	                          }//if SecondCommand IMSI     
	                  
	                        }
	                        else if (FirstCommand.equals("Exit")) 
	                        {					
	                             ExitClientChoice = true;
	     	 	        }//if First Command Exit
	    	    		
	 			//else if (FirstCommand.equals("Exit")) {					
	 			ExitClientChoice = true;
	 			//}//Exit
	 									
	    	        	}//while
	    	        	inClient.close();
	 			outClient.close();	
	 			m_bRunThread = false;	
	                }//while thread
	            }//try
	            catch(Exception e)
	            {
	                e.printStackTrace();
	            }//catch
	            finally
	            {
	                // Clean up
	                try
	                {    
	                    m_clientSocket.close();
	                    System.out.println("Client : ID - " + m_clientID + " - Stopped");
	                }
	                catch(IOException ioe)
	                {
	                    ioe.printStackTrace();
	                }//catch
	            }//finally
	        }//ClientServiceThread
	    }//run
	 }//class Server1