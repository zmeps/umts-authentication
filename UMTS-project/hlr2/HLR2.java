/*
*
*
* @authors                              Bazakas Athanasios 	icsdm11034 
* 					Gorogia Argyrw		icsdm11005
*					Karampelas Loukas	icsdm11008	
* 					Tsiatsikas Zisis 	icsdm11004	
*					
*/

package hlr2;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;


	public class HLR2 { 
		 
        //DB IMSI    
	private static String IMSI_table[][]= 
	{
		    {"IMSI/Loukas", "11,23,55,23,72,44,55,99,22,32,47,33,14,11,25,77"},
		    {"IMSI/Argyrw","71,77,45,47,12,17,35,49,124,16,41,61,19,32,22,23"},
                    {"IMSI/Thanasis","58,12,24,87,73,14,68,75,14,56,12,71,15,14,13,17"},
		    {"IMSI/zisis", "123,56,45,23,78,98,67,45,123,56,78,59,90,94,22,33"}
        
	};    
		    	
	private static short SQN[] = {0,0,0,0,0,0};//arxiko SQN
        private static short AMF[] = {0,0};//AMF
        
    public static void main(String[] args) throws IOException 
	   { 
	    ServerSocket serverSocket = null;	    
	    int Server2Port = 22222;

	    try { 
	         serverSocket = new ServerSocket(Server2Port); 
	        }//try
	    catch(IOException ioe)
        {
            System.out.println("Could not create server socket at "+Server2Port+". Quitting.");
            System.exit(-1);
        }//catch

	    Socket clientSocket = null; 
	    System.out.println ("Listening on port : "+Server2Port+"...");

	    try { 
	         clientSocket = serverSocket.accept(); 
	        }//try
	    catch (IOException e) 
	        { 
	         System.err.println("Accept failed."); 
	         System.exit(1); 
	        }//catch

	    System.out.println ("SGSN - Connected.");
	
	    ObjectInputStream in = null;
		ObjectOutputStream out = null;
		try{
			

           
            String dianisma_IMSI = null;
            String kleidi_dianismatos = "InvalidVector";
			
	        while (true) {
	        	
	        	in = new ObjectInputStream(clientSocket.getInputStream());
		        out = new ObjectOutputStream(clientSocket.getOutputStream());
		        
	
	        	String Server1Command=(String) in.readObject();
	        	//fakeloma    
	        	System.out.println("SGSN Sent : " + Server1Command);
		
	        	StringTokenizer token = new StringTokenizer(Server1Command,"##");
	        	Server1Command = null;
	        	Server1Command = token.nextToken();
		
                        
                        //SGSN sending Authentication_REQ
	        	if (Server1Command.equals("AUTHENTICATION_REQ")) 
                        {
                            
                            SQN[0]++;
                            if (SQN[0]==255) {SQN[0]=0;}
                            System.out.println("SQN : "+SQN[0]);
                            System.out.println("AMF : "+AMF[0]);
                            
                            dianisma_IMSI = token.nextToken();
                            //find dianisma and key
                            for (int i=0; i<IMSI_table.length; i++)//?????????
                            {
                            if(dianisma_IMSI.equals(IMSI_table[i][0]))//??????????????? 
                            {
                            
                            kleidi_dianismatos = IMSI_table[i][1];
                            System.out.println("Key from vector "+IMSI_table[i][0]+" is "+kleidi_dianismatos);
                            
                            short kleidi_short[] = new short[16];
                            StringTokenizer kleiditoken = new StringTokenizer(kleidi_dianismatos, ",");
                            for(i=0; i<16; i++){
                                kleidi_short[i] = Short.parseShort(kleiditoken.nextToken());
                            }
                            
                            short RAND[] = new short [16];
                            short AK[] = new short [6];
                            short AK_XOR_SQN[]= new short[6];
                            short IK[] = new short [16];
                            short CK[] = new short [16];
                            short XRES[] = new short [16];
                            short MAC[] = new short[8];
                               
                            short f2345_table[] = new short [54];
                          
                            /*Compoute RAND*/
                            String RANDSTRING = "";
                            RAND = functions.f0();
                            for (int j=0; j<16; j++)
                            {
                                RANDSTRING += RAND[j]+"$$";
                               //System.out.println("RAND["+j+"] = "+RAND[j]);
                            }//Rand string$$
                            
                            
                            f2345_table = functions.f2345(kleidi_short, RAND, XRES, CK, IK, AK);
                            MAC = functions.f1(kleidi_short, RAND, SQN, AMF);
                            
                            for(int f2345=0; f2345<54; f2345++)
                            {
                            // System.out.println(results[i]); 
                            //tmp += results[i]+"^";
                                if(f2345<6)
                                {
                                    AK[f2345] = f2345_table[f2345];
                                }
                                else if(f2345<22)
                                {
                                    IK[f2345-6] = f2345_table[f2345];
                           //tmp1 +=ik[i-6] + "%";
                                }
                                else if(f2345<38)
                                {
                                    CK[f2345-22] = f2345_table[f2345];   
                                }
                                else
                                {
                                    XRES[f2345-38] = f2345_table[f2345];
                                }
                            }//for f2345_table_table
                            
                            String CKSTRING = "";
                            for (int j=0; j<16; j++)
                            {
                                CKSTRING += CK[j]+"$$";
                            }//CK string$$
                            
                            
                            
                            String IKSTRING = "";
                            for (int j=0; j<16; j++)
                            {
                                IKSTRING += IK[j]+"$$";
                            }//IK string$$
                            
                            
                            
                            String XRESTRING = "";
                            for (int j=0; j<16; j++)
                            {
                               XRESTRING += XRES[j]+"$$";
                            }//CK string$$
                            
                            
                            for(int i_x=0; i_x<6; i_x++) {
                            AK_XOR_SQN[i_x] = (short) (AK[i_x] ^ SQN[i_x]);
                            }
                            ////AK_XOR_SQN
                            
                            String AK_XOR_SQN_STRING = "";
                            for (int j=0; j<6; j++)
                            {
                                AK_XOR_SQN_STRING +=  AK_XOR_SQN[j]+"$$";
                            }//AK_XOR_SQNstring$$
                            
                              
                            String AMF_STRING = "";
                            for (int j=0; j<2; j++)
                            {
                              AMF_STRING += AMF[j]+"$$";
                            }//AK_XOR_SQNstring$$
                            
                            String MAC_STRING = "";
                            for (int j=0; j<8; j++)
                            {
                               MAC_STRING +=  MAC[j]+"$$";
                            }//AK_XOR_SQNstring$$
                            
                            
                       
                            //Dimiourgeia token AUTN (AK_XOR_SQN, AMF, MAC)
                            String AUTNSTRING = AK_XOR_SQN_STRING+"@@"+AMF_STRING+"@@"+MAC_STRING;
                            
                              //Dimiourgeia token AuthenticationDataResposne (RAND, AUTN, XRES, CK, IK)
                            String AuthenticationDataResponse = RANDSTRING+"**"+AUTNSTRING+"**"+XRESTRING+"**"+CKSTRING+"**"+IKSTRING ;
                            System.out.println("AUTHENTICATIONDATARESPONSE - Sent to SGSN : (RAND ** AUTN ** XRES ** CK ** IK ** AUTN) : "+AuthenticationDataResponse);
                            out.writeObject(AuthenticationDataResponse);//sending to SGSN...
                            out.flush();
                           }//if     
                         
                    }//for
                    
                    //Lathos IMSI
                    if(kleidi_dianismatos.equals("InvalidVector"))
                    {
                        System.out.println("...Invalid Vector");
                        out.writeObject("InvalidVector");//kleidi_dianismatos="InvalidVector";////////////ston HLR 
		                out.flush();
                    }//Invalid vector
                    System.out.println("Authentication procedutre for vector : "+dianisma_IMSI+" complete...");
                    
                    
                }//if Authentication
	        	
	        	
	        	//closing
	        	//out.close(); 
	         	//in.close(); 
			}//while true
	       
		}//try
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }//catch
            finally
            {
	    // Clean up
            try
            {    
            	
            	clientSocket.close(); 
                System.out.println("SGSN - Stopped.");
            }//try
            catch(IOException ioe)
            {
                ioe.printStackTrace();
            }//catch
        }//finally
	   }//main 
	}//Class Server2
