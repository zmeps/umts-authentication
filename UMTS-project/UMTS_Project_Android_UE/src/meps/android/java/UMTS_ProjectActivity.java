package meps.android.java;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class UMTS_ProjectActivity extends Activity 
{

   EditText ip_sgsn;
   TextView textRAND,textAUTN, textOut, textRes, textTMSI;
   
   // My tokenzers
   StringTokenizer firstCommand_token, breakAutn_token, randtoken1, randtoken2;

   // My temporary strings
   private String tmp_firstCommand=""; 
   private String tmp_akXORsqn="", tmp_amf="", tmp_mac="", tmp="";
 
   // My authentication vector
   private String vectorAuth = "IMSI/zisis";
   //private String vectorAuth = "IMSI/Loukas";
   
   //My buttons
   Button buttoncredits;
  
   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) 
   {
	   
     super.onCreate(savedInstanceState);
     setContentView(R.layout.main);
  
     ip_sgsn = (EditText)findViewById(R.id.ip);
     Button buttonSend = (Button)findViewById(R.id.send);
     Button buttonEnd = (Button)findViewById(R.id.end);
     buttoncredits=(Button)findViewById(R.id.credits);
     textRAND = (TextView)findViewById(R.id.textRand);
     textAUTN = (TextView)findViewById(R.id.textAUTN);
     textRes = (TextView) findViewById(R.id.textRes);
     textOut = (TextView)findViewById(R.id.textout);
     textTMSI = (TextView)findViewById(R.id.tmsi);
     buttonSend.setOnClickListener(buttonSendOnClickListener);
     buttonEnd.setOnClickListener(buttonEndOnClickListener);
     buttoncredits.setOnClickListener(buttoncreditsOnClickListener);
     }
   
   Button.OnClickListener buttoncreditsOnClickListener = new Button.OnClickListener()
   {
	public void onClick(View arg0) {
		// prepare the alert box
	showCredits();}
	};

private void showCredits(){
	AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
    //set the title
    alertbox.setTitle("Credits");
    // set the message to display
    alertbox.setMessage("Ομάδα Προγραμματισμού \n\n1.Καράμπελας Λουκάς - icsdm11008 \n2.Μπαζάκας Αθανάσιος - icsdm11034" +
    		" \n3.Γκορόγια Αργυρώ - icsdm11005 \n4.Τσιάτσικας Ζήσης - icsdm11004" +
		"\n---------------------------------------\n" +
		"Το project υλοποιήθηκε στα πλαίσια του μαθήματος 'Aσφάλεια Ασύρματων & " +
		"Κινητών Δικτύων Επικοινωνιών', που διδάσκεται στην κατεύθυνση Ασφάλειας Πληροφοριακών " +
		"& Επικοινωνιακών Συστημάτων του μεταπτυχιακού προγράμματος σπουδών του " +
		"τμήματος Μηχανικών Πληροφοριακών & Επικοινωνιακών Συστημάτων του " +
		"Πανεπιστημίου Αιγαίου\n \n" +
		"Υπεύθυνος μαθήματος : Επικ. Καθηγητής, Καμπουράκης Γεώργιος\n" +
		"Υπεύθυνος εργαστηρίου : Υποψ. Διδάκτορας, Δαμόπουλος Δημήτριος\n" 
	
	);

    // add a neutral button to the alert box and assign a click listener
    alertbox.setPositiveButton("Back", new DialogInterface.OnClickListener() {

        // click listener on the alert box
        public void onClick(DialogInterface dialog, int which) {
            // the button was clicked
         }
    });
    // show it
    alertbox.show();
}
 Button.OnClickListener buttonSendOnClickListener = new Button.OnClickListener()
 {

    public void onClick(View arg0) 
    {
       // TODO Auto-generated method stub
    	if (ip_sgsn.getText().toString().equals(""))
    	{ Toast.makeText(getApplicationContext(), 
	            "Warning!!!You must insert IP of SGSN!", Toast.LENGTH_LONG).show();
     	}
    	else
    {
    	Socket socket = null;
       
       // Streams declaration
       ObjectOutputStream out;
       ObjectInputStream in;
       String FirstCommand="";
 
       //---------------------------------------------------------------
       // Arrays for computation results
       
       // User Key -----------------------------------------------------
       short Key[] = {123,56,45,23,78,98,67,45,123,56,78,59,90,94,22,33};
       //short Key[] = {123,56,45,23,78,98,47,45,123,56,78,59,91,94,22,34};
       short akXORsqn[] = new short[6]; 
       short results[] = new short[54];
       short Rand[] = new short[16];
       short x_mac[] = new short[8];
       short mac_s[] = new short[8];
       short Res[] = new short[16];
       short amf[] = new short[2];
       short sqn[] = new short[6];
       short ck[] = new short[16];
       short ik[] = new short[16];
       short ak[] = new short[6];
       
    //--------------------------------------------------------
    try 
    {
	 
       // Socket declaration
       socket = new Socket(ip_sgsn.getText().toString(), 11111);
       
       // Stream Objects
       out = new ObjectOutputStream(socket.getOutputStream());
       in = new ObjectInputStream(socket.getInputStream());
  
       // Sending authentication request + IMSI
       out.writeObject("Authentication##IMSI##"+vectorAuth) ;
       out.flush();
       textOut.setText("Stirng sent to SGSN = Authentication##IMSI##"+vectorAuth);
  
       do
       {
    	   // try-catch block for exceptions
    	   // ------------------------------
	       try 
	       {
	    	    // Read Input stream to take information from SGSN
	    	    // -----------------------------------------------
			    FirstCommand = (String)in.readObject();
			
	       } catch (ClassNotFoundException e) {
			   
	    	    // TODO Auto-generated catch block
			    e.printStackTrace();
	       }
	  
	       // Break initial String with { ** }
	       // --------------------------------------------------------
	       firstCommand_token = new StringTokenizer(FirstCommand,"**");
	       
           // RAND
	       // ----------------------------------------------
           tmp_firstCommand = firstCommand_token.nextToken(); 
           textRAND.setText("Rand receive = " + tmp_firstCommand);
           randtoken1 = new StringTokenizer(tmp_firstCommand,"$$");
      
           // BREAK RAND
           // -------------------
           for(int i=0; i<16; i++)
	       {
		      tmp = randtoken1.nextToken();
		      Rand[i] = Short.parseShort(tmp);
	       }
      
           // AUTN
           // ---------------------------------
           tmp = firstCommand_token.nextToken(); 
           breakAutn_token = new StringTokenizer(tmp,"@@");
           tmp_akXORsqn = breakAutn_token.nextToken();
           tmp_amf = breakAutn_token.nextToken();
           tmp_mac = breakAutn_token.nextToken();
           textAUTN.setText("AUTN = " + tmp);
           // sqnXORak Field 
           // -----------------------------------------------------------
           StringTokenizer tmp1 = new StringTokenizer(tmp_akXORsqn,"$$");	
           for(int i=0; i<6; i++)
           {
              tmp = tmp1.nextToken();
  		      akXORsqn[i] = Short.parseShort(tmp);
           }
       
           // amf Field 
           // ---------------------------------------
           tmp1 = new StringTokenizer( tmp_amf,"$$");
           for(int i=0; i<2; i++)
           {
      	      tmp = tmp1.nextToken();
   		      amf[i] = Short.parseShort(tmp);
           }
      
           // mac_s Filed
           // --------------------------------------
           tmp1 = new StringTokenizer(tmp_mac,"$$");
           for(int i=0; i<8; i++)
           {
      	      tmp = tmp1.nextToken();
   		      mac_s[i] = Short.parseShort(tmp);
           }
	  
           // Built rand string
           // -------------------
	       tmp="";
	       for(int i=0; i<16; i++)
	       {
		     tmp += Rand[i] + "%";  
	       }
	  
	       // Call f2345
	       // ---------------------------------------------------
	       results = functions.f2345(Key, Rand, Res, ck, ik, ak);
	  
	       tmp = "";
	       String verytmp="", Restmp="";
	  
	       // Load arrays from the result
	       // ---------------------------
	       for(int i=0; i<54; i++)
	       {
		     System.out.println(results[i]); 
		     tmp += results[i]+"^";
		  
		     if(i<6)
		     {
		         ak[i] = results[i];
		     }
		     else if(i<22)
		     {
			     ik[i-6] = results[i];
			     verytmp +=ik[i-6] + "%";
		     }
		     else if(i<38)
		     {
			     ck[i-22] = results[i];	  
		     }
		     else
		     {
			     Res[i-38] = results[i];
			     
		     }
	       }
	  
	       // xor to take sqn
	       // ------------------
	       for(int i=0; i<6; i++)
	       {
		      sqn[i] = (short) (akXORsqn[i] ^ ak[i]);
	       }
	  
	       // Compute x_mac
	       // ---------------------------------------------
	       x_mac = functions.f1(Key, Rand, sqn, amf, x_mac);
	  
	       // Check if mac_s == xmac
	       // ----------------------
	       verytmp="";
	       int flag=0;
	       for(int i=0; i<8; i++)
	       {
	    	  
		      if(mac_s[i] == x_mac[i])
		      {
		         verytmp+= x_mac[i] + "$$";
		      }
		      else
		      {
		    	  Toast.makeText(getApplicationContext(), 
		                  "Authentication Failed! Try again!", Toast.LENGTH_LONG).show(); 
			      flag=1;
		    	  break;
			     
		      }
	       }
	       
	       if(flag==1)
	       {
	    	   Toast.makeText(getApplicationContext(), 
	 	         "NETWORK PROBLEM AUTHENTICATION", Toast.LENGTH_LONG).show();
	    	   
	    	   textRes.setText("Network wasn't authenticated ... PROBLEM !");
	    	   out.writeObject("0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0") ;
		       out.flush();
	       }
	       else
	       {
	    	   
	    	   Toast.makeText(getApplicationContext(), 
	  	 	     "NETWORK AUTHENTICATION OK!", Toast.LENGTH_LONG).show(); 
	    	   
	    	   // Compute Res and create string to print
		       // --------------------
		       for(int i=0; i<16; i++)
		       {
			       Restmp+= Res[i] + "$$";  
		       }
		      
		       textRes.setText("Res sent to sgsn = " + Restmp);
		       out.writeObject(Restmp) ;
		       out.flush();
		    
		       /*
		       // Toast message....
		       // -----------------------------------
		       Toast.makeText(getApplicationContext(), 
	              "USER AUTHENTICATION OK!", Toast.LENGTH_LONG).show(); 
	           */
	       } 
	       
	        long meps = (Long) in.readObject();
	        Toast.makeText(getApplicationContext(), 
		            "Kasumi has sent :"+meps, Toast.LENGTH_LONG).show(); 
	 
	        long keyLeft = 1, keyRight = 1;
	        
	        for (int tmp=0; tmp<8; tmp++) {
                keyLeft = (keyLeft * ck[tmp]);
                }//keyleft
                
                for (int tmp=8; tmp<16; tmp++) {
                keyLeft = (keyRight * ck[tmp]);
                }//keyrigth
                
            Kasumi k = new Kasumi(keyLeft, keyRight);
            
            long Crypto_TMSI = k.decrypt(meps,8);
	        textTMSI.setText("Kasumi Decryption :"+Crypto_TMSI);
            
         }while(!FirstCommand.equals("END"));
	  
         in.close();
         out.close();
  
      } catch (UnknownHostException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      
      } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      } catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      finally
      {
      
    	  if (socket != null)
          {
          
            try 
            {
        	     socket.close();

            } catch (IOException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }
       }
     }}};
     
 Button.OnClickListener buttonEndOnClickListener = new Button.OnClickListener()
     {
        public void onClick(View arg0) 
        {
          // TODO Auto-generated method stub
        
        	if (ip_sgsn.getText().toString().equals(""))
        	{ Toast.makeText(getApplicationContext(), 
		            "Warning!!!You must insert IP of SGSN!", Toast.LENGTH_LONG).show();
         	}
        	else
        	{
        		Socket socket = null;
 	           
    	       	ObjectOutputStream out;
        		try {
        			
        	       	// Socket declaration
					socket = new Socket(ip_sgsn.getText().toString(), 11111);
					out = new ObjectOutputStream(socket.getOutputStream());
                       
	        		// 	Sending authentication request + IMSI
	        		out.writeObject("End");
	        		out.flush();
	        		out.close();
	        		Toast.makeText(getApplicationContext(), 
	    		            "Bye Bye!!!", Toast.LENGTH_LONG).show();
	        	
	        		finish();
	        		
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		        		
        	}
        	
     }};
}
        